﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HomeWork.Shape;

namespace HomeWork.Serialization
{
    class Context
    {
        public Context(IShape concreteShape)
        {
            ConcreteShape = concreteShape;
        }

        public IVisitor ConcreteVisitor { get; set; }
        public IShape ConcreteShape { get; set; }
        public void SetVisitor(IVisitor visitor)
        {
            ConcreteVisitor = visitor;
        }
        public void Serialize()
        {
            SetVisitor(ConcreteVisitor);
            ConcreteShape.Accept(ConcreteVisitor);
        }
        public string GetConcreteShapeClass(IShape concreteShape) 
        {
            return concreteShape.GetType().ToString().Replace("HomeWork.Shape.","");
        }
    }
}
