﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HomeWork.Shape
{
    class JSONSerialization : IVisitor
    {
        string IVisitor.CarreSerialization(Carre carre)
        {
            return $"{{\"Point1.X\":{carre.Point1.X},\"Point1.Y\":{carre.Point1.Y},\"Point2.X\":{carre.Point2.X},\"Point2.Y\":{carre.Point2.Y},\"Point3.X\":{carre.Point3.X},\"Point3.Y\":{carre.Point3.Y},\"Point4.X\":{carre.Point4.X},\"Point4.Y\":{carre.Point4.Y}}}";
        }

        string IVisitor.CircleSerialization(Circle circle)
        {
            return $"{{\"Center.X\":{circle.Center.X}, \"Center.Y\":{circle.Center.Y}, \"Radius\":{circle.Radius}}}";
        }

        string IVisitor.DotSerialization(Dot dot)
        {
            return $"{{\"Center.X\":{dot.Center.X}, \"Center.Y\":{dot.Center.Y}}}";
        }
    }
}
