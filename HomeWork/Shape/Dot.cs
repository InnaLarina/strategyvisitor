﻿using HomeWork.Serialization;
using System;
using System.Drawing;




namespace HomeWork.Shape
{
    class Dot : IShape
    {
        public Point Center { get; }

        public Dot(Point center)
        {
            Center = center;
        }
        public void Accept(IVisitor visitor)
        {
            Console.WriteLine(visitor.DotSerialization(this));
        }
    }
}
