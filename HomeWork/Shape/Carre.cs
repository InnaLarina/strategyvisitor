﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Drawing;
using HomeWork.Serialization;

namespace HomeWork.Shape
{
    class Carre:IShape
    {
        public Point Point1 { get; }
        public Point Point2 { get; }
        public Point Point3 { get; }
        public Point Point4 { get; }
        public Carre(Point point1, Point point2, Point point3, Point point4)
        {
            Point1 = point1;
            Point2 = point2;
            Point3 = point3;
            Point4 = point4;
        }

        public void Accept(IVisitor visitor)
        {
            Console.WriteLine(visitor.CarreSerialization(this));
        }
    }
}
