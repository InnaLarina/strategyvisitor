﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using HomeWork.Serialization;

namespace HomeWork.Shape
{
    interface IShape
    {
        void Accept(IVisitor visitor);
    }
}
