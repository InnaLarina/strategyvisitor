﻿using HomeWork.Serialization;

namespace HomeWork.Shape
{
    interface IVisitor
    {
        string DotSerialization(Dot shape);

        string CircleSerialization(Circle shape);

        string CarreSerialization(Carre shape);
    }
}
