﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HomeWork.Serialization;

namespace HomeWork.Shape
{
    class XMLSerialization : IVisitor
    {
        string IVisitor.CarreSerialization(Carre carre)
        {
            return "< Carre > " + Environment.NewLine +
             "< Point1 >" + Environment.NewLine +
             $"  < X > {carre.Point1.X} </ X >" + Environment.NewLine +
             $"  < Y > {carre.Point1.Y} </ Y >" + Environment.NewLine +
             "</ Point1 >" + Environment.NewLine +
             "< Point2 >" + Environment.NewLine +
             $"  < X > {carre.Point2.X} </ X >" + Environment.NewLine +
             $"  < Y > {carre.Point2.Y} </ Y >" + Environment.NewLine +
             "</ Point2 >" + Environment.NewLine +
             "< Point3 >" + Environment.NewLine +
             $"  < X > {carre.Point3.X} </ X >" + Environment.NewLine +
             $"  < Y > {carre.Point3.Y} </ Y >" + Environment.NewLine +
             "</ Point3 >" + Environment.NewLine +
             "< Point4 >" + Environment.NewLine +
             $"  < X > {carre.Point4.X} </ X >" + Environment.NewLine +
             $"  < Y > {carre.Point4.Y} </ Y >" + Environment.NewLine +
             "</ Point4 >" + Environment.NewLine +
              "</ Carre >";
        }

        string IVisitor.CircleSerialization(Circle circle)
        {
            return "< Circle > " + Environment.NewLine +
           "< Center >" + Environment.NewLine +
           $"  < X > {circle.Center.X} </ X >" + Environment.NewLine +
           $"  < Y > {circle.Center.Y} </ Y >" + Environment.NewLine +
           "</ Center >" + Environment.NewLine +
           $"< Radius > {circle.Radius} </ Radius >" + Environment.NewLine +
            "</ Circle >";
        }

        string IVisitor.DotSerialization(Dot dot)
        {
            return "< Dot > " + Environment.NewLine +
            "< Center >" + Environment.NewLine +
            String.Format("< X > {0} </ X >", dot.Center.X) + Environment.NewLine +
            String.Format("< Y > {0} </ Y >", dot.Center.Y) + Environment.NewLine +
            "</ Center >" + Environment.NewLine +
             "</ Dot >";
        }
    }
}
