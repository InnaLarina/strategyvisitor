﻿using HomeWork.Serialization;
using System;
using System.Drawing;

namespace HomeWork.Shape
{
    class Circle : IShape
    {
        public Point Center { get;}
        public int Radius { get; }
        public Circle(Point center, int radius)
        {
            this.Center = center;
            this.Radius = radius;
        }

        public void Accept(IVisitor visitor)
        {
            Console.WriteLine(visitor.CircleSerialization(this));
        }
    }
}
