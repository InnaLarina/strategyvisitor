﻿using System;
using System.Drawing;
using HomeWork.Shape;
using HomeWork.Serialization;

namespace HomeWork
{
    class Program
    {
        static void Main(string[] args)
        {

            Carre shape = new Carre(new Point(1, 2), new Point(3, 2), new Point(3, 0), new Point(1, 0));
            //Circle shape = new Circle(new Point(1,2),3);
            //Dot shape = new Dot(new Point(1, 2));
            Context context = new Context(shape);
            context.SetVisitor(new XMLSerialization());
            Console.WriteLine("XML Сериализация объекта класса "+ context.GetConcreteShapeClass(shape));
            Console.WriteLine();
            context.Serialize();
            
            Console.WriteLine();
            Console.WriteLine("JSON Сериализация объескта класса "+ context.GetConcreteShapeClass(shape));
            context.SetVisitor(new JSONSerialization());
            Console.WriteLine();
            context.Serialize();
            Console.ReadKey();
        }
    }
}
